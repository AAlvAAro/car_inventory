# README

### Setup the application with
  `bundle install`

  `rails db:prepare`
  `rails db:migrate`

### To run the application
  `rails s`

Application will be available at localhost:3000

### To run all the tests
  `rspec`

Or a specific set of tests can be run by adding the folder path to the command such as

  `rspec spec/models`
  `rspec spec/models/car_spec.rb`

