require 'rails_helper'

RSpec.describe Car, type: :model do
  attributes = %i[ brand model year price ]
  attributes.each do |attr|
    it { should validate_presence_of attr }
  end

  it { should validate_numericality_of(:price).is_greater_than(0) }
  it { should belong_to :dealership }
end
