class Car < ApplicationRecord
  belongs_to :dealership

  validates_presence_of :brand, :model, :year, :price
  validates_numericality_of :price, greater_than: 0
end
