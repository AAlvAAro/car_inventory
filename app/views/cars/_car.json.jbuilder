json.extract! car, :id, :brand, :model, :year, :price, :dealership_id, :created_at, :updated_at
json.url car_url(car, format: :json)
