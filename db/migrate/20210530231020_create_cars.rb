class CreateCars < ActiveRecord::Migration[6.1]
  def change
    create_table :cars do |t|
      t.string :brand
      t.string :model
      t.string :year
      t.string :price
      t.references :dealership, null: false, foreign_key: true, precision: 8, scale: 2

      t.timestamps
    end
  end
end
