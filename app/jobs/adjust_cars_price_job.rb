class AdjustCarsPriceJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Car.all.each do |car|
      price = car.price
      new_price = car.price * 0.98
      car.update(price: new_price)
    end
  end
end
