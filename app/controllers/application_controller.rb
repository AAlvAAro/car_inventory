class ApplicationController < ActionController::Base
  def after_sign_in_path_for(resource)
    root_path if resource.is_a? User

    dealerships_path
  end
end
