class Dealership < ApplicationRecord
  belongs_to :admin

  has_many :cars

  validates_presence_of :name, :address
end
