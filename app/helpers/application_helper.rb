module ApplicationHelper
  def formatted_price(price)
    "$ #{price}"
  end
end
