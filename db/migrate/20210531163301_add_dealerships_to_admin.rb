class AddDealershipsToAdmin < ActiveRecord::Migration[6.1]
  def change
    add_reference :dealerships, :admin, foreign_key: true, index: true
  end
end
