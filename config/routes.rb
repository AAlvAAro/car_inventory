Rails.application.routes.draw do
  devise_for :admins
  devise_for :users

  root to: 'cars#index'

  resources :cars

  resources :dealerships do
    resources :cars
  end
end
