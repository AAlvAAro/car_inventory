require 'rails_helper'

RSpec.describe Dealership, type: :model do
  it { should validate_presence_of :name }
  it { should validate_presence_of :address }
  it { should belong_to :admin }
  it { should have_many :cars }
end
